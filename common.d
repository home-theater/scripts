module common;

import std;

alias f = format;

struct GitPath
{
  string url;
  string directory = "/";
}

void shell(string cmd, string file = __FILE__, size_t line = __LINE__)
{
  auto result = executeShell(cmd);

  string errormsg()
  {
    string s = f!"command %s failed: (shell called from %s on line %s)\n"(cmd, file, line);
    foreach(line; result.output.lineSplitter)
      s ~= "> " ~ line ~ "\n";
    return s;
  }
  enforce(result.status == 0, errormsg);
}

void rmdirIfExists(string dir)
{
  if (exists(dir))
    rmdirRecurse(dir);
}


class MyException : Exception
{
  this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) pure nothrow @nogc @safe
  {
    super(msg, file, line, nextInChain);
  }
}
