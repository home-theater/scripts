#!/usr/bin/env rdmd
import std;
import common;

import config;

static assert (__traits(compiles, addon_name), "addon_name not set");
static assert (__traits(compiles, addon_version), "addon_version not set");
static assert (__traits(compiles, template_engine_config), "template_engine_config not set");

int main(string[] args)
{
  if (args.length != 1)
  {
    writeln("Usage: ./build_and_package_addon.d");
    return 1;
  }
  // {"defines": [], "substitutions": {}}
  JSONValue config = parseJSON(template_engine_config);

  enforce(addon_name.length, "addon_name is empty");
  enforce(addon_version.length, "addon_name is empty");

  string vars;
  if ("defines" in config)
    foreach (JSONValue define; config["defines"].array)
      vars ~= define.str ~ ",";
  if ("substitutions" in config)
    foreach (string key, JSONValue value; config["substitutions"].object)
      vars ~= key ~ "=" ~ value.str ~ ",";
  if (vars.endsWith(","))
    vars = vars[0 .. $ - 1];

  writeln("vars: ", vars);

  if (exists("build"))
    rmdirRecurse("build");
  mkdir("build");

  string[] buildFileList;

  foreach (string filePath; dirEntries(addon_name, SpanMode.depth))
  {
    string newFilePath = "build" ~ filePath[cast(long) addon_name.length .. $];
    if (filePath.endsWith(".template") && filePath.length > ".template".length)
    {
      newFilePath = newFilePath[0 .. $ - ".template".length];
      auto result = executeShell(f!"template-engine %s %s %s"(filePath, newFilePath, vars));
      enforce(result.status == 0, f!"template-engine failed: %s"(result.output));
    }
    else
    {
      copy(filePath, newFilePath);
    }
    buildFileList ~= newFilePath;
  }

  ZipArchive zip = new ZipArchive();
  foreach (file; buildFileList)
  {
    assert(exists(file));
    if (!isDir(file))
    {
      ArchiveMember member = new ArchiveMember;
      member.name = addon_name ~ file["build".length .. $];
      member.compressionMethod = CompressionMethod.deflate;
      if (!isDir(file))
        member.expandedData(cast(ubyte[]) read(file));
      zip.addMember(member);
    }
  }
  string zipName = f!"%s-%s.zip"(addon_name, addon_version);
  std.file.write(zipName, zip.build);
  writeln("zip file: ", zipName);

  return 0;
}
