# Scripts

Scripts used throughout the build process.
They are typically downloaded from this repository before usage.

## Note

`create_repository.d` uses this script internally: https://raw.githubusercontent.com/chadparry/kodi-repository.chad.parry.org/master/tools/create_repository.py