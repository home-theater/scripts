#!/usr/bin/env rdmd
import std;
import common;

import config;

static if (!__traits(compiles, template_engine_config))
  string template_engine_config = `{"defines": [], "substitutions": {}}`;

static if (!__traits(compiles, template_engine_addon_git_paths))
  GitPath[] template_engine_addon_git_paths = []; // These get built as zips -> addons/ folder using the build_and_package_addon.sh script

static if (!__traits(compiles, regular_addon_git_paths))
  GitPath[] regular_addon_git_paths = []; // These get cloned and their gitpath.directory placed in the addons/ folder (named after the repo)

static if (!__traits(compiles, regular_addon_zip_urls))
  string[] regular_addon_zip_urls = []; // These get downloaded in the addons/ folder

static if (!__traits(compiles, repo_addon_id))
  string repo_addon_id;

enum create_repo_py_url =
  "https://raw.githubusercontent.com/chadparry/kodi-repository.chad.parry.org/master/tools/create_repository.py";

int main(string[] args)
{
  if (!(args.length == 1))
  {
    writeln("Usage: ./create_repository.d");
    return 1;
  }

  rmdirIfExists("addons");
  mkdir("addons");

  // Build template engine addons
  foreach (gitpath; template_engine_addon_git_paths)
  {
    writeln("Building ", gitpath.url);

    // Clone repo
    rmdirIfExists("addon-repo");
    shell(f!"git clone --recurse-submodules \"%s\" addon-repo"(gitpath.url));

    // Build addon zip
    {
      auto cwd = getcwd;
      scope (exit)
        chdir(cwd);

      chdir("addon-repo");
      string dir = "./" ~ gitpath.directory;
      enforce(dir.exists && dir.isDir, f!"directory %s doesn't exist in gitpath %s"(dir, gitpath));
      chdir(dir);

      File("config.d", "ab").writeln(
        f!"\nstring template_engine_config = `%s`;"(template_engine_config)
      );
      shell("./build_and_package_addon.sh");
    }

    // Move addon zip
    auto zips = dirEntries("addon-repo", "*.zip", SpanMode.shallow);
    enforce(!zips.empty, f!"No zip was produced by building %s"(gitpath.url));
    string zip = zips.front.name;
    copy(zip, "addons/" ~ baseName(zip));
    rmdirRecurse("addon-repo");
  }

  foreach (gitpath; regular_addon_git_paths)
  {
    string name = gitpath.url.split("/")[$ - 1];
    writeln("Adding ", name);
    shell(f!"git clone --recurse-submodules \"%s\" addon-repo"(gitpath.url));
    shell(f!"mv addon-repo/%s addons/%s"(gitpath.directory, name));
    rmdirIfExists("addon-repo");
  }

  // Download extra addons
  foreach (url; regular_addon_zip_urls)
  {
    string name = url.split("/")[$ - 1];
    writeln("Adding ", name);
    enforce(name.canFind(".zip"), f!"Zip name %s doesn't contain '.zip' in url %s"(name, url));
    download(url, "addons/" ~ name);
  }

  // Build repository directory
  {
    // Download create_repository.py
    download(create_repo_py_url, "create_repository.py");
    scope (exit)
      std.file.remove("create_repository.py");
    shell("chmod +x create_repository.py");

    // Create repository tree
    rmdirIfExists("repository");
    shell("./create_repository.py --datadir=repository addons/*");
  }

  rmdirRecurse("addons");

  // Create zips subdir
  if (repo_addon_id.length)
  {
    chdir("repository");

    auto zips = dirEntries(repo_addon_id, f!"%s*.zip"(repo_addon_id), SpanMode.shallow);
    enforce(!zips.empty, f!"No addon zip was found in folder %s"(repo_addon_id));
    string zip = zips.front.name;

    mkdir("zip");
    shell(f!"cp %s zip/"(zip));
    File("zip/index.html", "wb").writeln(f!"<a href=\"%s\">%s</a>"(zip.baseName, zip.baseName));
  }

  return 0;
}
